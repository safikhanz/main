//7Digital API wrapper 
var oauthSignature = require('oauth-signature')
var axios = require('axios')
const fs = require('fs')
const qs = require('qs');
const { sevendigital_oauth_consumer_key, sevendigital_oauth_signature_method,sevendigital_oauth_version,sevendigital_country,sevendigital_consumerSecret} = require("../../config");


var oauthtimestamp = (Math.floor(Date.now() / 1000)).toString();
var oauthnonce = (Math.floor(Math.random()*100000000)).toString();

const generateSignaturedownload = async (url, httpMethod, nonce, timestamp, trackID) => {
    try {
        parameters = {
            
            oauth_consumer_key : sevendigital_oauth_consumer_key,
            oauth_nonce : nonce,
            oauth_timestamp : timestamp,
            oauth_signature_method : sevendigital_oauth_signature_method,
            oauth_version : sevendigital_oauth_version,
            country: sevendigital_country,
            trackId: trackID,
            formatId: '7',
    
        },
        consumerSecret = sevendigital_consumerSecret,
        // generates a RFC 3986 encoded, BASE64 encoded HMAC-SHA1 hash
        encodedSignature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret);
        return encodedSignature
    }
    catch(error){
console.log(error)
    }
}
const generateSignature = async (url, httpMethod, nonce, timestamp) => {
  try {
      parameters = {
          
          oauth_consumer_key : sevendigital_oauth_consumer_key,
          oauth_nonce : nonce,
          oauth_timestamp : timestamp,
          oauth_signature_method : sevendigital_oauth_signature_method,
          oauth_version : sevendigital_oauth_version,
          country: sevendigital_country,
 
  
      },
      consumerSecret = sevendigital_consumerSecret,
      // generates a RFC 3986 encoded, BASE64 encoded HMAC-SHA1 hash
      encodedSignature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret);
      return encodedSignature
  }
  catch(error){
console.log(error)
  }
}

const createPlaylist = async () => {}
const updatePlaylist = async (arrayOfSongs) => {
  try{
  const playlistUrl = 'https://api.7digital.com/1.2/playlists/6144f73116e06e3ef8d5748b/tracks';
  const playlistId = '6144f73116e06e3ef8d5748b'
  const signature = await generateSignature('https://api.7digital.com/1.2/playlists/6144f73116e06e3ef8d5748b/tracks', 'POST', oauthnonce,oauthtimestamp)
  const res = await axios.post('https://api.7digital.com/1.2/playlists/6144f73116e06e3ef8d5748b/tracks', arrayOfSongs, { params: {
    country: sevendigital_country,
    oauth_consumer_key: sevendigital_oauth_consumer_key,
    oauth_nonce: oauthnonce,
    oauth_signature_method: sevendigital_oauth_signature_method,
    oauth_timestamp: oauthtimestamp,
    oauth_version: sevendigital_oauth_version,
    oauth_signature: signature
}})
//console.log(res)
  }
  catch(error){console.log(error)}

}
const getPlaylistIdByName = async () => {}
const getPlaylistItemsById = async () => {
  try{
  const playlistUrl = 'https://api.7digital.com/1.2/playlists/6144f73116e06e3ef8d5748b/tracks';
  const signature = await generateSignature('https://api.7digital.com/1.2/playlists/6144f73116e06e3ef8d5748b/tracks', 'GET', oauthnonce, oauthtimestamp)
  const res = await axios.get('https://api.7digital.com/1.2/playlists/6144f73116e06e3ef8d5748b/tracks', { params: {
        country: sevendigital_country,
        oauth_consumer_key: sevendigital_oauth_consumer_key,
        oauth_nonce: oauthnonce,
        oauth_signature_method: sevendigital_oauth_signature_method,
        oauth_timestamp: oauthtimestamp,
        oauth_version: sevendigital_oauth_version,
        oauth_signature: signature
    }});
    //console.log(res.data.tracks)
    let data =[]

    res.data.tracks.forEach(items => {
      data.push({
        trackId: items.trackId,
        trackTitle: items.trackTitle,
        artistAppearsAs:items.artistAppearsAs,
        releaseId:items.releaseId,
        releaseTitle: items.releaseTitle,
        releaseArtistAppearsAs:items.artistAppearsAs
      }
        )
    })
    return data;

  }catch(error){
    console.log(error)
  }
}
const createUsers = async () => {}
const downloadSongsFromPlaylist = async (trackID) =>{
  try{
  const signature = await generateSignaturedownload('https://stream.svc.7digital.net/media/transfer','GET',oauthnonce,oauthtimestamp, trackID )
  const song = axios.get('https://stream.svc.7digital.net/media/transfer',
  { 
    responseType: 'stream',
    params: {
    trackId: trackID,
    formatId: '7',
    country: sevendigital_country,
    oauth_consumer_key: sevendigital_oauth_consumer_key,
    oauth_nonce: oauthnonce,
    oauth_signature_method: sevendigital_oauth_signature_method,
    oauth_timestamp: oauthtimestamp,
    oauth_version: sevendigital_oauth_version,
    oauth_signature: signature
  }}
  )
  return song
}

catch(error){
  console.log(error)
}
}
const checkSongAvailibity = async () => {}
const logBackSalesSevenDigital = async () => {}
const searchSongSevenDigital = async (searchKey) => {
  try{
    const signature = await generateSignature('https://api.7digital.com/1.2/track/search', 'GET',oauthnonce,oauthtimestamp )
    const res = await axios.get('https://api.7digital.com/1.2/track/search',
    { headers: {
      'Accept':  'application/json'
    }, 
    params: {
        q:searchKey,
        oauth_consumer_key: sevendigital_oauth_consumer_key,
        country: 'US',
        usageTypes: 'download,subscriptionstreaming,adsupportedstreaming'
    }});
    let dataReturn = res.data.searchResults.searchResult
    var songS=[]
    dataReturn.forEach(item => {
      songS.push({
        trackId: item.track.id,
        trackTitle: item.track.title,
        trackVersion: item.track.version,
        artistAppearsAs:item.track.artist.appearsAs,
        releaseId:item.track.release.id,
        releaseTitle: item.track.release.title,
        releaseArtistAppearsAs:item.track.release.artist.appearsAs,
        releaseVersion:item.track.release.version
          }
        )
    })

        //console.log(res.data)
        return songS
  }
  catch(error){
    console.log(error)
  }
}

const compareSongs = async (spotifySongDetails, sevenDigitalSongDetails) => {
  try{
  if (spotifySongDetails.trackTitle === sevenDigitalSongDetails.trackTitle){
    console.log('title matched')
    if(spotifySongDetails.trackAlbumTitle === sevenDigitalSongDetails.releaseTitle){
      console.log('album matched')
      if(spotifySongDetails.trackArtist === sevenDigitalSongDetails.artistAppearsAs){
        console.log('artist matched')
        return true;
      }
    }
  }
} catch(error) {
  console.log(error)
}
 
}

(async () => {
  try{
    const arrayofSongs =  [ {
      "trackId": "100176082",
      "trackTitle": "All the Things",
      "artistAppearsAs": "Mary Bue",
      "releaseId": "11938761",
      "releaseTitle": "The World Is Your Lover",
      "releaseArtistAppearsAs": "Mary Bue",
      "releaseVersion": "Explicit"
   
}]
//const tracks = await updatePlaylist(arrayofSongs);
//console.log(tracks)
const writer = fs.createWriteStream('./util/song.mp3');
const res = await downloadSongsFromPlaylist('100176079');
console.log(res)
res.data.pipe(fs.createWriteStream('./util/song.mp3'))

  }
  catch(error){
    console.log(error)
  }

})(); 
module.exports = {
  downloadSongsFromPlaylist,
  createUsers,
  getPlaylistItemsById,
  getPlaylistIdByName,
  updatePlaylist,
  checkSongAvailibity,
  logBackSalesSevenDigital,
  searchSongSevenDigital,
  createPlaylist,
  compareSongs
}






 
