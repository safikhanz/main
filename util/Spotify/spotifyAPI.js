//Spotify API wrapper 


const axios = require("axios");
const qs = require('qs');
const {   spotify_client_id, spotify_client_secret, spotify_token } = require('../../config');


var client_id = spotify_client_id;
var client_secret = spotify_client_secret;
var url= 'https://accounts.spotify.com/api/token';
var data1 = qs.stringify({
  'grant_type': 'client_credentials',
  'json': 'true' 
});
var token = Buffer.from(client_id + ':' + client_secret);
var base64token = token.toString('base64');

var config11 = {

  headers: { 
    'Authorization': `Basic ${spotify_token}`, 
    'Content-Type': 'application/x-www-form-urlencoded'
  }
};


const getPlaylistSongs = async () => {
  try{
    const res = await axios.post(url, data1,config11);
    console.log(res.data.access_token)
    access_token = res.data.access_token
    var config = {
  
      headers: { 
        'Authorization': `Bearer ${access_token}`, 
        'Content-Type': 'application/json'
      }
    };
    const res1 = await axios.get('https://api.spotify.com/v1/playlists/7D8vEXoIifQw9WQs7v9zC4/tracks',config)
    //console.log(res1.data.items[0].track.name)
    var data=res1.data.items
    var spotifySongs=[]
    data.forEach(item => {
    //console.log(item.track.artists)
    spotifySongs.push({
      trackTitle: item.track.name,
      trackId: item.track.id,
      trackAlbumTitle: item.track.album.name,
      trackAlbumId: item.track.album.id,  
      trackArtist: (item.track.artists).map((items) => {return items.name})
      
    })
  })
  
  return  spotifySongs
    
    }
    catch(error){
      console.log("error: " +error)
    }
}
/*
(async () => {
  try{
    const res = await axios.post(url, data1,config11 );
    console.log(res.data.access_token)
    access_token = res.data.access_token
    var config1 = {
  
      headers: { 
        'Authorization': `Bearer ${access_token}`, 
        'Content-Type': 'application/json'
      }
    };
    const res1 = await axios.get('https://api.spotify.com/v1/playlists/4hMirEeYeS7IWtxJ6wWT7h/tracks',config1)
    console.log(res1.data.items[0].track.name)
    data123=res1.data.items
    var songs=[]
    data123.forEach(item => {
    //console.log(item.track.artists)
    
        songs.push({
          trackTitle: item.track.name,
          trackId: item.track.id,
          trackAlbumTitle: item.track.album.name,
          trackAlbumId: item.track.album.id,
      
          trackArtist: (item.track.artists).map((items) => {return items.name})
          
  
          
        })
  })
  console.log(songs)
  return songs
    
    }
    catch(error){
      console.log("error: " +error)
    }
})(); */

module.exports = {getPlaylistSongs}
